<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * Allow other modules to alter PHRETS Search parameters.
 *
 * @param $data
 */
function hook_real_estate_rets_search_alter(&$data) {
  $data['recursive'] = TRUE;
}


/**
 * Allow other modules to alter PHRETS GetObject parameters.
 *
 * @param $data
 */
function hook_real_estate_rets_get_object_alter(&$data) {
  $data['type'] = 'Photo';
}
