<?php

namespace Drupal\real_estate_rets;

use Drupal\real_estate_property\PropertyProcessor;

/**
 * Process query rets information.
 */
class RetsProcessor extends PropertyProcessor implements RetsProcessorInterface {

  /**
   * Name of fetch queue to use.
   *
   * @var string
   */
  protected $fetchQueueName = 'real_estate_rets_fetch_tasks';

  /**
   * Name of item queue to use.
   *
   * @var string
   */
  protected $itemQueueName = 'real_estate_rets_item_tasks';

}
