<?php

namespace Drupal\real_estate_rets;

use Drupal\real_estate_property\PropertyFetcherInterface;

/**
 * Fetches property information from RETS server.
 */
interface RetsFetcherInterface extends PropertyFetcherInterface {

}
