<?php

namespace Drupal\real_estate_rets\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\real_estate_rets\RetsFetcher;
use Drupal\real_estate_rets\RetsManagerInterface;
use Drupal\Core\Config\ConfigManager;

/**
 * Class RetsManualLoad.
 */
class RetsManualLoad extends FormBase {

  /**
   * Drupal\real_estate_rets\RetsFetcher definition.
   *
   * @var \Drupal\real_estate_rets\RetsFetcher
   */
  protected $realEstateRetsFetcher;
  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  /**
   * Update manager service.
   *
   * @var \Drupal\update\UpdateManagerInterface
   */
  protected $retsManager;

  /**
   * Constructs a new RetsManualLoad object.
   */
  public function __construct(
    RetsFetcher $real_estate_rets_fetcher,
    ConfigManager $config_manager,
    RetsManagerInterface $rets_manager
  ) {
    $this->realEstateRetsFetcher = $real_estate_rets_fetcher;
    $this->configManager = $config_manager;
    $this->retsManager = $rets_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('real_estate_rets.fetcher'),
      $container->get('config.manager'),
      $container->get('real_estate_rets.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'real_estate_rets_manual_load';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#markup' => '<p>' . $this->t('Load RETS data manually.') . '</p>',
    ];

    if ($items = $this->retsManager->getQueries()) {
      $options = [];
      $default = [];
      foreach ($items as $item) {
        foreach ($item['queries'] as $key => $query) {
          $index = $item['id'] . '|' . $key;
          $options[$index] = Html::escape($item['label']) . ' - ' . Html::escape($query['label']);
          $default[] = $index;
        }
      }
      asort($options);
    }

    if (empty($options)) {
      $form['message'] = [
        '#type' => 'markup',
        '#markup' => $this->t('No queries found.'),
      ];
    }
    else {
      $form['queries'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Queries to process'),
        '#options' => $options,
        '#default_value' => $default ?? NULL,
      ];
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Load'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $queries = [];
    if (!empty($form_state->getValue('queries'))) {
      foreach ($form_state->getValue('queries') as $key => $value) {
        if (!$value) {
          continue;
        }
        $parts = explode('|', $key);
        $queries[$parts[0]][$parts[1]] = TRUE;
      }
    }

    if ($queries) {
      $this->retsManager->refreshData($queries);
    }
    else {
      // No queries selected.
      // Could not show error so we can just process any already-queued items.
      \Drupal::messenger()->addWarning(t('No queries selected.'));
      return;
    }

    $batch = [
      'operations' => [
        [['\Drupal\real_estate_rets\RetsManager', 'fetchDataBatchProxy'], []],
//        [['\Drupal\real_estate_rets\RetsManager', 'itemBatchProxy'], []],
      ],
      'finished' => 'rets_fetch_data_finished',
      'title' => $this->t('Load RETS Data'),
      'progress_message' => $this->t('Processed RETS sources...'),
      'error_message' => $this->t('Error loading RETS data.'),
    ];
    batch_set($batch);

  }

}
