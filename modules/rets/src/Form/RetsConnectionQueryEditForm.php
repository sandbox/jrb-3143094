<?php

namespace Drupal\real_estate_rets\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class RetsConnectionQueryEditForm.
 */
class RetsConnectionQueryEditForm extends EntityForm {

  /**
   * The ID of the query that is being edited.
   *
   * @var string
   */
  protected $queryId;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'connect_query_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $connection_query = NULL) {
    $this->queryId = $connection_query;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /* @var \Drupal\real_estate_rets\Entity\RetsConnectionInterface $connect */
    $connect = $this->getEntity();
    /** @var \Drupal\real_estate_rets\RetsQuery $query */
    $query = $connect->getQuery($this->queryId);

    // @todo - Use dependency injection for State.
    $state = \Drupal::service('state');
    if ($last_run_timestamp = $state->get('rets_last_run|' . $connect->id() . '|' . $this->queryId)) {
      $last_run = date('Y-m-d', $last_run_timestamp);
    }
    else {
      $last_run = $this->t('never');
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $query->label(),
      '#description' => $this->t('Label for the query.'),
      '#required' => TRUE,
    ];
    $form['resource'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Resource'),
      '#maxlength' => 255,
      '#default_value' => $query->resource(),
      '#description' => $this->t('Resource for the query.'),
      '#required' => TRUE,
    ];
    $form['class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Class'),
      '#maxlength' => 255,
      '#default_value' => $query->class(),
      '#description' => $this->t('Class for the query.'),
      '#required' => TRUE,
    ];
    $form['dmql'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dmql'),
      '#maxlength' => 255,
      '#default_value' => $query->dmql(),
      '#description' => $this->t('Default value: DMQL2 .'),
      '#required' => TRUE,
    ];
    $form['query'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Query'),
      '#maxlength' => 255,
      '#default_value' => $query->query(),
      '#description' => $this->t('Ex.: "(DATE_MODIFIED=2018-07-01-2020-08-01),(PHOTO_COUNT=1-5)" (without quotes)<br/>The token <em>RETS_LAST_RUN</em> will be replaced with the date of the last run of this query (@last_run).', ['@last_run' => $last_run]),
      '#required' => TRUE,
    ];
    $form['format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Format'),
      '#maxlength' => 255,
      '#default_value' => $query->format(),
      '#description' => $this->t('Default value: COMPACT . Possible values: COMPACT, COMPACT-DECODED, ..etc.'),
      '#required' => TRUE,
    ];
    $form['limit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Limit'),
      '#maxlength' => 255,
      '#default_value' => $query->limit(),
      '#description' => $this->t('Limit.'),
      '#required' => TRUE,
    ];
    $form['recursive'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use recursive query?'),
      '#description' => $this->t('Note: This must be supported by the RETS server.'),
      '#default_value' => $query->recursive(),
    ];
    $form['standardnames'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Standard Names'),
      '#maxlength' => 25,
      '#default_value' => $query->standardnames(),
      '#description' => $this->t('Default value: 0 . Possible values: 0, 1 etc.'),
      '#required' => TRUE,
    ];
    $options = [3600, 10800, 21600, 43200, 86400, 604800];
    $form['cron_interval'] = [
      '#type' => 'select',
      '#title' => t('Run query via cron every'),
      '#description' => t('Run this query via Cron. Drupal cron must be properly configured.'),
      '#default_value' => $query->cronInterval(),
      '#options' => [0 => t('Never')] + array_map([\Drupal::service('date.formatter'), 'formatInterval'], array_combine($options, $options)),
    ];
    $options = [86400, 172800, 345600, 604800, 1209600, 2592000];
    $form['status_cron_interval'] = [
      '#type' => 'select',
      '#title' => t('Run status update via cron every'),
      '#description' => t('Run status update via Cron. Drupal cron must be properly configured.'),
      '#default_value' => $query->statusCronInterval(),
      '#options' => [0 => t('Never')] + array_map([\Drupal::service('date.formatter'), 'formatInterval'], array_combine($options, $options)),
    ];
    $form['get_objects'] = [
      '#type' => 'checkbox',
      '#title' => t('Retrieve objects with query'),
      '#description' => t('If set, GetObject will be called for each item returned by RETS.'),
      '#default_value' => $query->getobjects(),
    ];
    $form['object_type'] = [
      '#type' => 'textfield',
      '#title' => t('GetObject type'),
      '#description' => t('The "type" to be sent with the GetObject call.'),
      '#default_value' => $query->objecttype(),
    ];
    $form['mapping'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Mapping'),
    ];
    $form['mapping']['entity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity'),
      '#maxlength' => 255,
      '#default_value' => $query->entity(),
      '#description' => $this->t('Entity.'),
      '#required' => TRUE,
    ];
    $form['mapping']['key_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key Field'),
      '#maxlength' => 255,
      '#default_value' => $query->keyField(),
      '#description' => $this->t('Key Field.'),
      '#required' => TRUE,
    ];
    $form['mapping']['status_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Status Field'),
      '#maxlength' => 255,
      '#default_value' => $query->statusField(),
      '#description' => $this->t('Status Field.'),
      '#required' => TRUE,
    ];
    $form['mapping']['select'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Select'),
      '#default_value' => $query->select(),
      '#description' => $this->t('Mapped fields list for downloading. Each record must be on a separate line and look like "rets_field:drupal_field_name". For example:
<pre>
BuildingProjectName:field_building_name
StreetNume:field_address/address_line1
ListPrice:field_price
</pre>'),
      '#required' => TRUE,
      '#rows' => 30,
    ];
    $form['id'] = [
      '#type' => 'value',
      '#value' => $this->queryId,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\real_estate_rets\Entity\RetsConnectionInterface $connect */
    // $connect = $this->getEntity();
    // $values = $form_state->getValues();
    // todo: add validation.
  }

  /**
   * Copies top-level form values to entity properties.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity the current form should operate upon.
   * @param array $form
   *   A nested array of form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    if (!$form_state->isValidationComplete()) {
      // Only do something once form validation is complete.
      return;
    }
    /** @var \Drupal\real_estate_rets\Entity\RetsConnectionInterface $entity */
    $values = $form_state->getValues();
    $form_state->set('created_query', FALSE);
    $entity->setQueryLabel($values['id'], $values['label']);
    $entity->setQueryResource($values['id'], $values['resource']);
    $entity->setQueryClass($values['id'], $values['class']);
    $entity->setQueryQuery($values['id'], $values['query']);
    $entity->setQueryDmql($values['id'], $values['dmql']);
    $entity->setQueryFormat($values['id'], $values['format']);
    $entity->setQueryLimit($values['id'], $values['limit']);
    $entity->setQueryRecursive($values['id'], $values['recursive']);
    $entity->setQueryStandardNames($values['id'], $values['standardnames']);
    $entity->setQueryCronInterval($values['id'], $values['cron_interval']);
    $entity->setQueryStatusCronInterval($values['id'], $values['status_cron_interval']);
    $entity->setQueryKeyField($values['id'], $values['key_field']);
    $entity->setQueryStatusField($values['id'], $values['status_field']);
    $entity->setQueryGetObjects($values['id'], $values['get_objects']);
    $entity->setQueryObjectType($values['id'], $values['object_type']);
    $entity->setQueryEntity($values['id'], $values['entity']);
    $entity->setQuerySelect($values['id'], $values['select']);
    if (isset($values['type_settings'])) {
      $configuration = $entity->getTypePlugin()->getConfiguration();
      $configuration['queries'][$values['id']] = $values['type_settings'][$entity->getTypePlugin()->getPluginId()];
      $entity->set('type_settings', $configuration);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\real_estate_rets\Entity\RetsConnectionInterface $connect */
    $connect = $this->entity;
    $connect->save();
    $this->messenger()->addStatus($this->t('Saved %label query.', [
      '%label' => $connect->getQuery($this->queryId)->label(),
    ]));
    $form_state->setRedirectUrl($connect->toUrl('queries-list'));
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#submit' => ['::submitForm', '::save'],
    ];

    $actions['delete'] = [
      '#type' => 'link',
      '#title' => $this->t('Delete'),
      // Deleting a query is editing a connect.
      '#access' => $this->entity->access('edit'),
      '#attributes' => [
        'class' => ['button', 'button--danger'],
      ],
      '#url' => Url::fromRoute('entity.real_estate_rets_connection.delete_query_form', [
        'real_estate_rets_connection' => $this->entity->id(),
        'connection_query' => $this->queryId,
      ]),
    ];

    return $actions;
  }

}
