<?php

namespace Drupal\real_estate_rets;

use Drupal\real_estate_property\PropertyQueryInterface;

/**
 * A query object.
 */
interface RetsQueryInterface extends PropertyQueryInterface {

  /**
   * Gets the query's resource.
   *
   * @return string
   *   The query's resource.
   */
  public function resource();

  /**
   * Gets the query's class.
   *
   * @return string
   *   The query's class.
   */
  public function class();

  /**
   * Gets the query's value.
   *
   * @return string
   *   The query's value.
   */
  public function query();

  /**
   * Gets the query's dmql.
   *
   * @return string
   *   The query's dmql.
   */
  public function dmql();

  /**
   * Gets the query's format.
   *
   * @return string
   *   The query's format.
   */
  public function format();

  /**
   * Gets the query's limit.
   *
   * @return string
   *   The query's limit.
   */
  public function limit();

  /**
   * Gets the query's recursive setting.
   *
   * @return string
   *   The query's limit.
   */
  public function recursive();

  /**
   * Gets the query's standardnames.
   *
   * @return string
   *   The query's standardnames.
   */
  public function standardnames();

}
