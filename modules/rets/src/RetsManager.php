<?php

namespace Drupal\real_estate_rets;

use Drupal\real_estate_property\PropertyManager;

/**
 * Default implementation of RetsManagerInterface.
 */
class RetsManager extends PropertyManager implements RetsManagerInterface {

  /**
   * Name of config object to load.
   *
   * @var string
   */
  protected $configObjectName = 'rets.settings';

  /**
   * Entity type ID to use.
   *
   * @var string
   */
  protected $entityTypeId = 'real_estate_rets_connection';

  /**
   * {@inheritdoc}
   */
  public function getManagerTypeText() {
    return $this->t('RETS');
  }

  public static function fetchDataBatchProxy(array &$context) {
    \Drupal::getContainer()->get('real_estate_rets.manager')
      ->fetchDataBatch($context);
  }

  public static function itemBatchProxy(array &$context) {
    \Drupal::getContainer()->get('real_estate_rets.manager')
      ->itemBatch($context);
  }

}
