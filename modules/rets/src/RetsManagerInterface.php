<?php

namespace Drupal\real_estate_rets;

use Drupal\real_estate_property\PropertyManagerInterface;

/**
 * Manages rets information.
 */
interface RetsManagerInterface extends PropertyManagerInterface {

}
