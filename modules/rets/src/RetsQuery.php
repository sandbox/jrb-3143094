<?php

namespace Drupal\real_estate_rets;

use Drupal\real_estate_property\PropertyQuery;
use Drupal\real_estate_rets\Entity\RetsConnectionInterface;

/**
 * A query value object.
 */
class RetsQuery extends PropertyQuery implements RetsQueryInterface {

  /**
   * The connection that this query is connected to.
   *
   * @var \Drupal\real_estate_rets\Entity\RetsConnectionInterface
   */
  private $connection;

  /**
   * The query's resource.
   *
   * @var string
   */
  private $resource;

  /**
   * The query's class.
   *
   * @var string
   */
  private $class;

  /**
   * The query's value.
   *
   * @var string
   */
  private $query;

  /**
   * The query's dmql.
   *
   * @var string
   */
  private $dmql;

  /**
   * The query's format.
   *
   * @var string
   */
  private $format;

  /**
   * The query's limit.
   *
   * @var string
   */
  private $limit;

  /**
   * The query's recursive setting.
   *
   * @var string
   */
  private $recursive;

  /**
   * If objects should be retrieved with query.
   *
   * @var bool
   */
  private $getobjects;

  /**
   * The type to use for retrieving objects.
   *
   * @var string
   */
  private $objecttype;

  /**
   * The query's StandardNames.
   *
   * @var string
   */
  private $standardnames;

  /**
   * RetsQuery constructor.
   *
   * @param \Drupal\real_estate_rets\Entity\RetsConnectionInterface $connection
   *   The connection the query is attached to.
   * @param string $id
   *   The query's ID.
   * @param string $label
   *   The query's label.
   * @param string $weight
   *   The query's weight.
   * @param string $resource
   *   The query's resource.
   * @param string $class
   *   The query's class.
   * @param string $query
   *   The query's value.
   * @param string $dmql
   *   The query's dmql.
   * @param string $format
   *   The query's format.
   * @param string $limit
   *   The query's limit.
   * @param string $recursive
   *   The query's recursive setting.
   * @param string $standardnames
   *   The query's standardnames.
   * @param string $cron_interval
   *   The query's cron_interval.
   * @param string $status_cron_interval
   *   The query's status_cron_interval.
   * @param string $key_field
   *   The query's key_field.
   * @param string $status_field
   *   The query's status_field.
   * @param boolean $get_objects
   *   If objects should be retrieved with query.
   * @param string $object_type
   *   The type to use for retrieving objects.
   * @param string $entity
   *   The query's entity.
   * @param string $select
   *   The query's select.
   */
  public function __construct(RetsConnectionInterface $connection, $id, $label, $weight, $resource, $class, $query, $dmql, $format, $limit, $recursive, $standardnames, $cron_interval, $status_cron_interval, $key_field, $status_field, $get_objects, $object_type, $entity, $select) {
    $this->connection = $connection;
    $this->resource = $resource;
    $this->class = $class;
    $this->query = $query;
    $this->dmql = $dmql;
    $this->format = $format;
    $this->limit = $limit;
    $this->recursive = $recursive;
    $this->getobjects = $get_objects;
    $this->objecttype = $object_type;
    $this->standardnames = $standardnames;
    parent::__construct($id, $label, $weight, $cron_interval, $status_cron_interval, $key_field, $status_field, $entity, $select);
  }

  /**
   * {@inheritdoc}
   */
  public function resource() {
    return $this->resource;
  }

  /**
   * {@inheritdoc}
   */
  public function class() {
    return $this->class;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    return $this->query;
  }

  /**
   * {@inheritdoc}
   */
  public function dmql() {
    return $this->dmql;
  }

  /**
   * {@inheritdoc}
   */
  public function format() {
    return $this->format;
  }

  /**
   * {@inheritdoc}
   */
  public function limit() {
    return $this->limit;
  }

  /**
   * {@inheritdoc}
   */
  public function recursive() {
    return $this->recursive;
  }

  /**
   * {@inheritdoc}
   */
  public function standardnames() {
    return $this->standardnames;
  }

  /**
   * {@inheritdoc}
   */
  public function getobjects() {
    return $this->getobjects;
  }

  /**
   * {@inheritdoc}
   */
  public function objecttype() {
    return $this->objecttype;
  }

}
