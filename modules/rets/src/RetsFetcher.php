<?php

namespace Drupal\real_estate_rets;

use Drupal\Core\Queue\RequeueException;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use PHRETS\Configuration;
use Drupal\real_estate_property\PropertyFetcher;
use PHRETS\Models\BaseObject;
use Drupal\Core\File\FileSystemInterface;

/**
 * Fetches a data from RETS server.
 */
class RetsFetcher extends PropertyFetcher implements RetsFetcherInterface {

  use DependencySerializationTrait;

  protected $connect;

  protected $rets;

  const EARLIEST_DATE = '1950-01-01';
  const LAST_RUN_DATE_FORMAT = 'Y-m-d';

  /**
   * {@inheritdoc}
   */
  public function fetchData(array $query) {

    $data = [];

    // Reconnect if isn't connected to this RETS connection.
    if ($this->connect != $query['id']) {
      if (!$this->connectRetsServer($query)) {
        // Could throw a RequeueException here, but that would force it to keep
        // trying this same queue item over an over which would prevent other
        // queue items from being processed.
//        throw new RequeueException();
        return FALSE;
      }
    }

    try {

      // @todo - Use dependency injection for State.
      $state = \Drupal::service('state');
      $state_key = 'rets_last_run|' . $query['id'] . '|' . $query['query_id'];
      if (!$query['status_update'] && $last_run_timestamp = $state->get($state_key)) {
        $last_run = date(self::LAST_RUN_DATE_FORMAT, $last_run_timestamp);
      }
      else {
        $last_run = self::EARLIEST_DATE;
      }

      // Replace our date token with the last time this query was run.
      $query['query'] = str_replace('RETS_LAST_RUN', $last_run, $query['query']);

      // Fields, that will be fetched. Input string can look like:
      // ListPrice:field_price
      // CityName:field_city'
      // Make it look like 'ListPrice,CityName'.
      $select = [];

      if ($query['status_update']) {
        // Only get the status.
        $query['select'] = $query['status_field'];
      }
      $query['select'] .= "\n" . $query['key_field'];
      $select_mapping = $this->parseSelectMapping($query['select']);
      foreach ($select_mapping as $info) {
        $select[] = $info['source'];
      }
      $select = implode(',', array_filter($select));

      // Get the key field alone.
      $key_field = trim(preg_replace('/:\w+/', '', $query['key_field']));

      $results = $this->rets->Search(
        $query['resource'],
        $query['class'],
        $query['query'],
        [
          'QueryType' => $query['dmql'],
          // Count and records.
          'Count' => 1,
          'Format' => $query['format'],
          'Limit' => $query['limit'],
          // Give system names.
          'StandardNames' => (int) $query['standardnames'],
          'Select' => $select,
        ],
        $query['recursive']
      );

      if ($results) {


//        print 'RETS returned results count: '  . $results->getReturnedResultsCount() . "\n";


        /** @var \PHRETS\Models\Search\Record( $row */
        foreach ($results as $row) {
          $result_row = [];
          $result_row[$key_field] = $row->get($key_field);
          foreach ($select_mapping as $info) {
            if (empty($info['source'])) {
              $value = NULL;
            }
            else {
              $value = $row->get($info['source']);
            }
            $this->runFieldProcessors($value, $info);
            $result_row[$info['destination']] = $value;
          } // Loop thru mapped fields.

          $result_row['objects'] = [];
          if (empty($query['status_update']) && !empty($query['get_objects']) && !empty($query['object_type']) && !empty($result_row[$key_field])) {
            $result_row['objects'] = $this->getObjects($query, $query['object_type'], $result_row[$key_field]);
          } // Need to get objects?

          $data[] = $result_row;

        } // Loop thru rows in results.
      }

      if (empty($query['status_update'])) {
        // Update State with the last time this query was run.
        $state->set($state_key, \Drupal::time()->getRequestTime());
      }

      // var_dump($data);
    }
    catch (\Exception $exception) {
      watchdog_exception('real_estate_rets', $exception);
//      throw new RequeueException();
    }

    // Return mapping info too.
    $data_set = [
      'class' => self::class,
      'entity' => $query['entity'],
      'key_field' => $query['key_field'],
      'status_update' => $query['status_update'] ?? FALSE,
      'select' => $select_mapping ?? NULL,
      'data' => $data,
    ];



    $num = count($data);
//    print "rets returned {$num} items\n";



    // Allow other modules to alter full fetched data set.
    \Drupal::moduleHandler()->alter('real_estate_property_data_set', $data_set);

    return $data_set;
  }

  protected function getObjects($query, $object_type, $key, $login = FALSE) {

    $out = [];

    try {

      if ($login) {
        $this->connectRetsServer($query);
      }

      /** @var \Illuminate\Support\Collection $objects */
      if ($objects = $this->rets->GetObject('Property', $object_type, $key, '*', 1)) {
        /** @var \PHRETS\Models\BaseObject $object */
        foreach ($objects as $object) {

          $is_temp_file = FALSE;
          if (!$object->getLocation() && $object->getContent()) {
            if ($temp_file = $this->saveTempFile($key, $object)) {
              $is_temp_file = TRUE;
              $object->setLocation($temp_file);
            } // Content saved locally?
          } // Have the content in the response?

          $out[] = [
            'content_id' => $object->getContentId(),
            'object_id' => $object->getObjectId(),
            'content_type' => $object->getContentType(),
            'url' => $object->getLocation(),
            'alt' => $object->getContentDescription(),
            'is_temp_file'=> $is_temp_file,
          ];

        } // Loop thru objects.
      } // Got objects?

    }
    catch (\Exception $e) {
      drupal_set_message('Error retrieving photos: ' . $e->getMessage(), 'warning', TRUE);
      if (!$login) {
        $out = $this->getObjects($query, $object_type, $key);
      }
    }

    return $out;

  }

  protected function saveTempFile($key, BaseObject $object) {

    if (strpos($object->getContentType(), 'application/xml') !== FALSE) {
      return FALSE;
    } // Is it an XML response (which it would be for an error)?

    $path = 'public://rets-tmp/' . date('YmdHi') . '/' . md5($key . '|' . uniqid()) . '/';
    $parts = explode('/', $object->getContentType());
    $filename = $object->getContentId() . '-' . $object->getObjectId() . '.' . ($parts[1] ?? '');

    // @todo Inject this dependency.
    /** @var \Drupal\Core\File\FileSystem $filesystem */
    $filesystem = \Drupal::service('file_system');
    if ($filesystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      $file = $filesystem->saveData($object->getContent(), $path . $filename);
    }
    return $file ?? FALSE;

  }

  /**
   * {@inheritdoc}
   */
  protected function connectRetsServer($query) {
    try {

      // Setup configuration. Used \PHRETS\Configuration.
      $config = new Configuration();
      $config->setLoginUrl($query['login_url']);
      $config->setUsername($query['username']);
      $config->setPassword($query['password']);

      $config->setRetsVersion($query['rets_version']);
      $config->setUserAgent($query['user_agent']);
      $config->setUserAgentPassword($query['user_agent_password']);
      $config->setHttpAuthenticationMethod($query['http_authentication']);
      $config->setOption('use_post_method', $query['use_post_method']);
      $config->setOption('disable_follow_location', $query['disable_follow_location']);

      // Get a session ready using the configuration. Used \PHRETS\Session.
      $this->rets = new RetsSession($config);

      $this->connect = FALSE;
      // Make the first request.
      if ($this->rets->Login()) {
        $this->connect = $query['id'];
      }

    }
    catch (\Exception $exception) {
      watchdog_exception('real_estate_rets', $exception);
      return FALSE;
    }
    return TRUE;
  }

}
