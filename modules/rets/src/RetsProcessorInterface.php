<?php

namespace Drupal\real_estate_rets;

use Drupal\real_estate_property\PropertyProcessorInterface;

/**
 * Processor of rets data.
 */
interface RetsProcessorInterface extends PropertyProcessorInterface {

}
