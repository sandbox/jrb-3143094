<?php

namespace Drupal\real_estate_rets\Plugin\QueueWorker;

use Drupal\real_estate_property\Plugin\QueueWorker\PropertyQueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines 'real_estate_rets_item_tasks' queue worker for items queued by manual form.
 *
 * @QueueWorker(
 *   id = "real_estate_rets_item_tasks",
 *   title = @Translation("RETS Property Item Manual Queue Worker"),
 * )
 */
class RetsItemManualQueueWorker extends PropertyQueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('real_estate_rets.processor')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $this->propertyProcessor->processItemTask($data);
  }

}
