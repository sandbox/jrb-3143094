<?php

namespace Drupal\real_estate_rets\Plugin\QueueWorker;

use Drupal\real_estate_property\Plugin\QueueWorker\PropertyQueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines 'real_estate_rets_fetch_tasks_cron' queue worker.
 *
 * @QueueWorker(
 *   id = "real_estate_rets_fetch_tasks_cron",
 *   title = @Translation("RETS Property Fetch Queue Worker"),
 * )
 */
class RetsFetchQueueWorker extends PropertyQueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('real_estate_rets.processor.cron')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    // @todo - Use dependency injection here.
    $query_type = (!empty($data['status_update'])) ? t('Status only') : t('Full update');
    \Drupal::logger('real_estate_rets')->debug('Processing queue item for @id - @query_id (@type).', ['@id' => $data['id'], '@query_id' => $data['query_id'], '@type' => $query_type]);
    $this->propertyProcessor->processFetchTask($data);
  }

}
