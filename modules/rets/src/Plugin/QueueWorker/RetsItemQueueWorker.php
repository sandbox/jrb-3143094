<?php

namespace Drupal\real_estate_rets\Plugin\QueueWorker;

use Drupal\real_estate_property\Plugin\QueueWorker\PropertyQueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines 'real_estate_rets_item_tasks_cron' queue worker.
 *
 * @QueueWorker(
 *   id = "real_estate_rets_item_tasks_cron",
 *   title = @Translation("RETS Property Item Queue Worker"),
 * )
 */
class RetsItemQueueWorker extends PropertyQueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('real_estate_rets.processor.cron')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $this->propertyProcessor->processItemTask($data);
  }

}
