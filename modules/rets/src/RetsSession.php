<?php

namespace Drupal\real_estate_rets;

use PHRETS\Session;

/**
 * Class RetsSession
 *
 * This class exists to extend the PHRETS Session class so that we can run some
 * Drupal alter functions before making RETS calls.
 *
 * @package Drupal\real_estate_rets
 */
class RetsSession extends Session {

  /**
   * {@inheritdoc}
   */
  public function Search($resource_id, $class_id, $dmql_query, $optional_parameters = [], $recursive = false) {
    // Allow other modules to alter search parameters.
    $data = [
      'resource_id' => $resource_id,
      'class_id' => $class_id,
      'dmql_query' => $dmql_query,
      'optional_parameters' => $optional_parameters,
      'recursive' => $recursive,
    ];
    \Drupal::moduleHandler()->alter('real_estate_rets_search', $data);
    return parent::Search($data['resource_id'], $data['class_id'], $data['dmql_query'], $data['optional_parameters'], $data['recursive']);
  }

  /**
   * {@inheritdoc}
   */
  public function GetObject($resource, $type, $content_ids, $object_ids = '*', $location = 0) {
    // Allow other modules to alter parameters.
    $data = [
      'resource' => $resource,
      'type' => $type,
      'content_ids' => $content_ids,
      'object_ids' => $object_ids,
      'location' => $location,
    ];
    \Drupal::moduleHandler()->alter('real_estate_rets_get_object', $data);
    return parent::GetObject($data['resource'], $data['type'], $data['content_ids'], $data['object_ids'], $data['location']);
  }

}
