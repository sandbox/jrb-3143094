<?php

/**
 * @file
 * Contains real_estate_openimmo.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function real_estate_openimmo_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the real_estate_openimmo module.
    case 'help.page.real_estate_openimmo':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Integration with OpenImmo.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Batch callback.
 *
 * @param bool $success
 *   TRUE if the batch operation was successful;
 *   FALSE if there were errors.
 * @param array $results
 *   An associative array of results from the batch operation.
 */
function openimmo_fetch_data_finished($success, array $results) {
  if ($success) {
    if (!empty($results)) {
      if (!empty($results['queries']['updated'])) {
        \Drupal::messenger()->addStatus(\Drupal::translation()->formatPlural($results['queries']['updated'], 'Loaded OpenImmo data for one query.', 'Loaded OpenImmo data for @count queries.'));
      }
      if (!empty($results['queries']['failures'])) {
        \Drupal::messenger()->addError(\Drupal::translation()->formatPlural($results['queries']['failures'], 'Failed to get OpenImmo data for one query.', 'Failed to get OpenImmo data for @count queries.'));
      }
      if (!empty($results['items']['updated'])) {
        \Drupal::messenger()->addStatus(\Drupal::translation()->formatPlural($results['items']['updated'], 'Loaded OpenImmo data for one item.', 'Loaded OpenImmo data for @count items.'));
      }
      if (!empty($results['items']['failures'])) {
        \Drupal::messenger()->addError(\Drupal::translation()->formatPlural($results['items']['failures'], 'Failed to get OpenImmo data for one item.', 'Failed to get OpenImmo data for @count items.'));
      }
    }
  }
  else {
    \Drupal::messenger()->addError(t('An error occurred trying to get OpenImmo data.'));
  }
}

/**
 * Implements hook_cron().
 */
function real_estate_openimmo_cron() {
  /** @var \Drupal\real_estate_openimmo\OpenImmoManager $openimmo_manager */
  $openimmo_manager = \Drupal::service('real_estate_openimmo.manager.cron');
  if ($queries_processed = real_estate_property_queue_property_fetching($openimmo_manager, 'openimmo_query_cron')) {
    \Drupal::logger('real_estate_openimmo')->info(t('OpenImmo queries processed and queued via cron: @count.', ['@count' => $queries_processed]));
  }
}
