<?php

namespace Drupal\real_estate_openimmo;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\real_estate_property\PropertyFetcher;
use GuzzleHttp\Exception\RequestException;

/**
 * Fetches a data from OpenImmo server.
 */
class OpenImmoFetcher extends PropertyFetcher implements OpenImmoFetcherInterface {

  use DependencySerializationTrait;

  /**
   * {@inheritdoc}
   */
  public function fetchData(array $query) {

    $data = [];

    $path = $query['feed_config']['file_path'] ?? NULL;
    if (!$path) {
      // @todo - throw error.
      return $data;
    }

    try {

      if (!file_exists($path)) {
        // @todo - throw error.
        return $data;
      }

      $contents = file_get_contents($path);

      $xpath = new \DOMXPath($this->createDomDocument($contents));
      $this->registerNamespaces($xpath);

      $select_mapping = $this->parseSelectMapping($query['select']);

      foreach ($xpath->query($query['row_selector']) as $row) {

        $key_field = explode(':', $query['key_field'])[0];

        $result_row = [];
        $result_row['row_key_value'] = $this->executeRowQuery($xpath, $key_field, $row)[0] ?? NULL;

        foreach ($select_mapping as $info) {
          if (empty($info['source'])) {
            $value = NULL;
          }
          else {
            $value = $this->executeRowQuery($xpath, $info['source'], $row)[0] ?? NULL;
          }
          $this->runFieldProcessors($value, $info);
          $result_row[$info['destination']] = $value;
        } // Loop thru mapped fields.

        $data[] = $result_row;

      } // Loop thru row.

    }
    catch (RequestException $exception) {
      watchdog_exception('real_estate_openimmo', $exception);
    }

    // Return mapping info too.
    $data_set = [
      'class' => self::class,
      'entity' => $query['entity'],
      'key_field' => $query['key_field'],
      'select' => $select_mapping,
      'data' => $data,
    ];

    // Allow other modules to alter full fetched data set.
    \Drupal::moduleHandler()->alter('real_estate_property_data_set', $data_set);

    return $data_set;

  }

  /**
   * Creates a very forgiving DOMDocument.
   *
   * From  \Drupal\views_xml_backend\Plugin\views\query\Xml
   *
   * @param string $contents
   *   The XML content of the DOMDocument.
   *
   * @return \DOMDocument
   *   A new DOMDocument.
   */
  protected function createDomDocument($contents) {

    // Try to make the XML loading as forgiving as possible.
    $document = new \DOMDocument();
    $document->strictErrorChecking = FALSE;
    $document->resolveExternals = FALSE;
    // Libxml specific.
    $document->substituteEntities = TRUE;
    $document->recover = TRUE;

    $options = LIBXML_NONET;

    if (defined('LIBXML_COMPACT')) {
      $options |= LIBXML_COMPACT;
    }
    if (defined('LIBXML_PARSEHUGE')) {
      $options |= LIBXML_PARSEHUGE;
    }
    if (defined('LIBXML_BIGLINES')) {
      $options |= LIBXML_BIGLINES;
    }

    // @see http://symfony.com/blog/security-release-symfony-2-0-11-released
    $disable_entities = libxml_disable_entity_loader(TRUE);

    $document->loadXML($contents, $options);

    // @see http://symfony.com/blog/security-release-symfony-2-0-17-released
    foreach ($document->childNodes as $child) {
      if ($child->nodeType === XML_DOCUMENT_TYPE_NODE) {

        // @todo Add more context. The specific view? A link to the page?
//        $this->logger->error('A suspicious document was detected.');

        // Overwrite the document to allow processing to continue.
        $document = new \DOMDocument();
        break;
      }
    }

    libxml_disable_entity_loader($disable_entities);

    return $document;
  }

  /**
   * Registers available namespaces.
   *
   * From  \Drupal\views_xml_backend\Plugin\views\query\Xml
   *
   * @param \DOMXPath $xpath
   *   The XPath object.
   */
  protected function registerNamespaces(\DOMXPath $xpath) {
    $xpath->registerNamespace('php', 'http://php.net/xpath');

    if (!$simple = @simplexml_import_dom($xpath->document)) {
      return;
    }

    foreach ($simple->getNamespaces(TRUE) as $prefix => $namespace) {
      $xpath->registerNamespace($prefix, $namespace);
    }
  }

  /**
   * Executes an XPath query on a given row.
   *
   * From  \Drupal\views_xml_backend\Plugin\views\query\Xml
   *
   * @param \DOMXPath $xpath
   *   The XPath object.
   * @param string $selector
   *   The XPath selector.
   * @param \DOMNode $row
   *   The row as.
   *
   * @return string[]
   *   Returns a list of values from the row.
   */
  protected function executeRowQuery(\DOMXPath $xpath, $selector, \DOMNode $row) {
    $node_list = @$xpath->query($selector, $row);

    if ($node_list === FALSE) {
      return [];
    }

    $values = [];
    foreach ($node_list as $node) {
      $values[] = $node->nodeValue;
    }

    return $values;
  }

}
