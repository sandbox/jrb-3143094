<?php

namespace Drupal\real_estate_openimmo\Plugin\QueueWorker;

use Drupal\real_estate_property\Plugin\QueueWorker\PropertyQueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines 'real_estate_openimmo_item_tasks_cron' queue worker.
 *
 * @QueueWorker(
 *   id = "real_estate_openimmo_item_tasks_cron",
 *   title = @Translation("OpenImmo Property Item Queue Worker"),
 *   cron = {"time" = 60}
 * )
 */
class OpenImmoItemQueueWorker extends PropertyQueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('real_estate_openimmo.processor.cron')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $this->propertyProcessor->processItemTask($data);
  }

}
