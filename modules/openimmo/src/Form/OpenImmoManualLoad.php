<?php

namespace Drupal\real_estate_openimmo\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\real_estate_openimmo\OpenImmoFetcher;
use Drupal\real_estate_openimmo\OpenImmoManager;
use Drupal\Core\Config\ConfigManager;

/**
 * Class OpenImmoManualLoad.
 */
class OpenImmoManualLoad extends FormBase {

  /**
   * Drupal\real_estate_openimmo\OpenImmoFetcher definition.
   *
   * @var \Drupal\real_estate_openimmo\OpenImmoFetcher
   */
  protected $realEstateOpenImmoFetcher;
  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  /**
   * Update manager service.
   *
   * @var \Drupal\update\UpdateManagerInterface
   */
  protected $openImmoManager;

  /**
   * Constructs a new OpenImmoManualLoad object.
   */
  public function __construct(
    OpenImmoFetcher $real_estate_openimmo_fetcher,
    ConfigManager $config_manager,
    OpenImmoManager $openimmo_manager
  ) {
    $this->realEstateOpenImmoFetcher = $real_estate_openimmo_fetcher;
    $this->configManager = $config_manager;
    $this->openImmoManager = $openimmo_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('real_estate_openimmo.fetcher'),
      $container->get('config.manager'),
      $container->get('real_estate_openimmo.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'real_estate_openimmo_manual_load';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#markup' => '<p>' . $this->t('Load OpenImmo data manually.') . '</p>',
    ];

    if ($items = $this->openImmoManager->getQueries()) {
      $options = [];
      $default = [];
      foreach ($items as $item) {
        foreach ($item['queries'] as $key => $query) {
          $index = $item['id'] . '|' . $key;
          $options[$index] = Html::escape($item['label']) . ' - ' . Html::escape($query['label']);
          $default[] = $index;
        }
      }
    }

    if (empty($options)) {
      $form['message'] = [
        '#type' => 'markup',
        '#markup' => $this->t('No queries found.'),
      ];
    }
    else {
      $form['queries'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Queries to process'),
        '#options' => $options,
        '#default_value' => $default ?? NULL,
      ];
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Load'),
      ];
    }

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $queries = [];
    if (!empty($form_state->getValue('queries'))) {
      foreach ($form_state->getValue('queries') as $key => $value) {
        if (!$value) {
          continue;
        }
        $parts = explode('|', $key);
        $queries[$parts[0]][$parts[1]] = TRUE;
      }
    }

    if ($queries) {
      $this->openImmoManager->refreshData($queries);
    }
    else {
      // No queries selected.
      // Could not show error so we can just process any already-queued items.
      \Drupal::messenger()->addWarning(t('No queries selected.'));
      return;
    }

    $batch = [
      'operations' => [
        [[$this->openImmoManager, 'fetchDataBatch'], []],
        [[$this->openImmoManager, 'itemBatch'], []],
      ],
      'finished' => 'openimmo_fetch_data_finished',
      'title' => $this->t('Load OpenImmo Data'),
      'progress_message' => $this->t('Processing OpenImmo sources...'),
      'error_message' => $this->t('Error loading OpenImmo data.'),
    ];
    batch_set($batch);

  }

}
