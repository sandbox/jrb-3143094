<?php

namespace Drupal\real_estate_openimmo;

use Drupal\real_estate_property\PropertyQueryInterface;

/**
 * A query object.
 */
interface OpenImmoQueryInterface extends PropertyQueryInterface {

  public function rowSelector();

}
