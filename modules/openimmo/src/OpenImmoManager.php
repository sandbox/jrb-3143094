<?php

namespace Drupal\real_estate_openimmo;

use Drupal\real_estate_property\PropertyManager;

/**
 * Default implementation of OpenImmoManager.
 */
class OpenImmoManager extends PropertyManager implements OpenImmoManagerInterface {

  /**
   * Name of config object to load.
   *
   * @var string
   */
  protected $configObjectName = 'openimmo.settings';

  /**
   * Entity type ID to use.
   *
   * @var string
   */
  protected $entityTypeId = 'real_estate_openimmo';

  /**
   * {@inheritdoc}
   */
  public function getManagerTypeText() {
    return $this->t('OpenImmo');
  }

}
