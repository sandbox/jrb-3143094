<?php

namespace Drupal\real_estate_openimmo;

use Drupal\real_estate_property\PropertyQuery;
use Drupal\real_estate_openimmo\Entity\OpenImmoInterface;

/**
 * A query value object.
 */
class OpenImmoQuery extends PropertyQuery implements OpenImmoQueryInterface {

  /**
   * The query's row_selector.
   *
   * @var string
   */
  private $rowSelector;

  /**
   * The source that this query is connected to.
   *
   * @var \Drupal\real_estate_openimmo\Entity\OpenImmoInterface
   */
  private $source;

  /**
   * OpenImmoQuery constructor.
   *
   * @param \Drupal\real_estate_openimmo\Entity\OpenImmoInterface $source
   *   The source the query is attached to.
   * @param string $id
   *   The query's ID.
   * @param string $label
   *   The query's label.
   * @param string $weight
   *   The query's weight.
   * @param string $cron_interval
   *   The query's cron_interval.
   * @param string $key_field
   *   The query's key_field.
   * @param string $entity
   *   The query's entity.
   * @param string $row_selector
   *   The query's row_selector.
   * @param string $select
   *   The query's select.
   */
  public function __construct(OpenImmoInterface $source, $id, $label, $weight, $cron_interval, $key_field, $entity, $row_selector, $select) {
    $this->source = $source;
    $this->rowSelector = $row_selector;;
    parent::__construct($id, $label, $weight, $cron_interval, $key_field, $entity, $select);
  }

  /**
   * {@inheritdoc}
   */
  public function rowSelector() {
    return $this->rowSelector;
  }

}
