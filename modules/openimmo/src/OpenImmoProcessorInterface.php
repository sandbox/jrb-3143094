<?php

namespace Drupal\real_estate_openimmo;

use Drupal\real_estate_property\PropertyProcessorInterface;

/**
 * Processor of OpenImmo data.
 */
interface OpenImmoProcessorInterface extends PropertyProcessorInterface {

}
