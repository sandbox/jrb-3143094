<?php

namespace Drupal\real_estate_openimmo;

use Drupal\real_estate_property\PropertyFetcherInterface;

/**
 * Fetches property information from OpenImmo source.
 */
interface OpenImmoFetcherInterface extends PropertyFetcherInterface {

}
