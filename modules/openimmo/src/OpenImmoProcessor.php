<?php

namespace Drupal\real_estate_openimmo;

use Drupal\real_estate_property\PropertyProcessor;

/**
 * Process query Open Immo information.
 */
class OpenImmoProcessor extends PropertyProcessor implements OpenImmoProcessorInterface {

  /**
   * Name of fetch queue to use.
   *
   * @var string
   */
  protected $fetchQueueName = 'real_estate_openimmo_fetch_tasks';

  /**
   * Name of item queue to use.
   *
   * @var string
   */
  protected $itemQueueName = 'real_estate_openimmo_item_tasks';

}
