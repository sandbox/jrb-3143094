<?php

namespace Drupal\real_estate_openimmo;

use Drupal\real_estate_property\PropertyManagerInterface;

/**
 * Manages OpenImmo information.
 */
interface OpenImmoManagerInterface extends PropertyManagerInterface {

}
