<?php

namespace Drupal\real_estate_property;

/**
 * Manages property information.
 */
interface PropertyManagerInterface {

  /**
   * Fetches an array of data.
   *
   * @param array|null $queries_to_run
   *
   * @return array
   *   Returns update data.
   */
  public function refreshData($queries_to_run = NULL, $status_update = FALSE);

  /**
   * Returns name of manager type to be used in messages.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Manager type.
   */
  public function getManagerTypeText();

  /**
   * Processes a step in batch for fetching data.
   *
   * In example is used result of query, but we use queue items instead.
   * https://api.drupal.org/api/drupal/core%21includes%21form.inc/group/batch/8.4.x.
   *
   * @param array $context
   *   Reference to an array used for Batch API storage.
   */
  public function fetchDataBatch(array &$context);

}
