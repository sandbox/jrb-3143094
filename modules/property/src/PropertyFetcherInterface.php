<?php

namespace Drupal\real_estate_property;

/**
 * Fetches property information from source.
 */
interface PropertyFetcherInterface {

  /**
   * Retrieves a data from source.
   *
   * @param array $query
   *   The array of query information.
   *
   * @return array
   *   The result of query to source. Empty string upon failure.
   */
  public function fetchData(array $query);

}
