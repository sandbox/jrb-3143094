<?php

namespace Drupal\real_estate_property;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\file\Entity\File;
use Drupal\Core\Queue\RequeueException;
use Drupal\Core\Queue\SuspendQueueException;

/**
 * Process query information.
 */
abstract class PropertyProcessor implements PropertyProcessorInterface {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The ProprtyFetcher service.
   *
   * @var \Drupal\real_estate_property\PropertyFetcherInterface
   */
  protected $fetcher;

  /**
   * The fetch queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $fetchQueue;

  /**
   * The item queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $itemQueue;

  /**
   * Array of release history URLs that we have failed to fetch.
   *
   * @var array
   */
  protected $failed;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The filesystem service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Name of fetch queue to use.  Override when extending this class.
   *
   * @var string
   */
  protected $fetchQueueName = 'real_estate_fetch_tasks';

  /**
   * Name of item queue to use.  Override when extending this class.
   *
   * @var string
   */
  protected $itemQueueName = 'real_estate_item_tasks';

  /**
   * Constructs a PropertyProcessor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Queue\QueueFactory $fetch_queue_factory
   *   The queue factory.
   *   The queue factory.
   * @param \Drupal\Core\Queue\QueueFactory $item_queue_factory
   *   The queue factory.
   * @param \Drupal\real_estate_property\PropertyFetcherInterface $fetcher
   *   The fetcher service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param bool|false $is_cron
   *   True if being used by cron.
   */
  public function __construct(ConfigFactoryInterface $config_factory, QueueFactory $fetch_queue_factory, QueueFactory $item_queue_factory, PropertyFetcherInterface $fetcher, EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, bool $is_cron = FALSE) {
    $this->fetcher = $fetcher;
    $this->config = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    if ($is_cron) {
      $this->fetchQueueName .= '_cron';
      $this->itemQueueName .= '_cron';
    }
    $this->fetchQueue = $fetch_queue_factory->get($this->fetchQueueName);
    $this->itemQueue = $item_queue_factory->get($this->itemQueueName);

  }

  /**
   * {@inheritdoc}
   */
  public function createFetchTask(array $query) {
    $this->fetchQueue->createItem($query);
  }

  /**
   * {@inheritdoc}
   */
  public function createItemTask(array $data) {
    $this->itemQueue->createItem($data);
  }

  /**
   * {@inheritdoc}
   */
  public function fetchData() {
    while ($item = $this->fetchQueue->claimItem()) {
      $this->processFetchTask($item->data);
      $this->fetchQueue->deleteItem($item);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setUpdateKey($source) {
    $key = md5($source . '||' . random_bytes(32));
    // @todo - Use dependency injection here.
    $db = \Drupal::database();
    $db->update('real_estate_property_field_data')
      ->fields(['update_key' => $key])
      ->condition('source', $source)
      ->execute();
    return $key;
  }

  /**
   * {@inheritdoc}
   */
  public function processFetchTask(array $query) {

    $num = 0;
    $success = FALSE;

    $update_key = NULL;
    $source = $query['id'] . '|' . $query['query_id'];
    if (!empty($query['status_update'])) {
      $update_key = $this->setUpdateKey($source);
      $status_update_state_key = 'rets_update_processing|' . $source;
      // @todo - Use dependency injection for State.
      /** @var \Drupal\Core\State\StateInterface $state */
      $state = \Drupal::service('state');

      if ($timestamp = $state->get($status_update_state_key)) {
        \Drupal::logger('real_estate_property')
          ->error(t('Status update is already running for query @query (since @date).', [
            '@query' => $query['id'] . '-' . $query['query_id'],
            '@date' => date('Y-m-d H:i:s', $timestamp),
          ]));
        return FALSE;
      }
      $state->set($status_update_state_key, \Drupal::time()->getRequestTime());
    }

//    print "\n\n\n===========\nsource: {$source}\n";
//    print "update_key: {$update_key}\n";

    $data = $this->fetcher->fetchData($query);
    if (!empty($data['data'])) {
      $success = TRUE;
      $entity_variable_array = explode(':', $data['entity']);
      $key_variable_array = explode(':', $data['key_field']);
      $status_variable_array = explode(':', $data['status_field']);
      $base_args = [
        'rets_id' => $query['id'],
        'query_id' => $query['query_id'],
        'source' => $source,
        'update_key' => $update_key,
        'class' => $data['class'],
        'entity_type' => $entity_variable_array[0],
        'entity_bundle' => $entity_variable_array[1],
        'key_field_source' => $key_variable_array[0],
        'key_field' => $key_variable_array[1],
        'status_field_source' => $key_variable_array[0],
        'status_field' => $status_variable_array[1],
        'status_update' => $query['status_update'] ?? FALSE,
        'select' => $data['select'],
      ];
      foreach ($data['data'] as $row) {
        $num++;
        $args = $base_args;
        $args['row'] = $row;
        if (!empty($query['status_update'])) {
          $this->updateEntityStatus($args);
        }
        else {
          $this->createItemTask($args);
        } // Status update only?
      } // Loop thru rows.

      if (!empty($query['status_update'])) {
        \Drupal::moduleHandler()->invokeAll('real_estate_property_post_status_update', [$base_args]);
      }

    } // Got data?
    // @todo - Use dependency injection here.

    if (!empty($query['status_update'])) {
      \Drupal::logger('real_estate_property')
        ->info(t('Checked status for (@num) property item(s) for query @query.', [
          '@num' => $num,
          '@query' => $query['id'] . '-' . $query['query_id']
        ]));
      $state->delete($status_update_state_key);
    }
    else {
      \Drupal::logger('real_estate_property')
        ->info(t('Queued (@num) property item(s) for processing for query @query.', [
          '@num' => $num,
          '@query' => $query['id'] . '-' . $query['query_id']
        ]));
    }

    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchItem() {
    while ($item = $this->itemQueue->claimItem()) {
      $this->processItemTask($item->data);
      $this->itemQueue->deleteItem($item);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processItemTask(array $item) {
    $status_update_state_key = 'rets_update_processing|' . $item['source'];
    // @todo - Use dependency injection for State.
    /** @var \Drupal\Core\State\StateInterface $state */
    $state = \Drupal::service('state');
    if ($timestamp = $state->get($status_update_state_key)) {
      \Drupal::logger('real_estate_property')
        ->warning(t('Will not process RETS item because status update is already running for query @query (since @date).', [
          '@query' => $item['rets_id'] . '-' . $item['query_id'],
          '@date' => date('Y-m-d H:i:s', $timestamp),
        ]));
      // Status update is running for this query, so we need to not do any item
      // updates until it's done. We throw a SuspendQueueException here, but
      // this will stop all item queue processing (even for other queries). Not
      // sure what else we can do?
      // We could throw a RequeueException here, but that would force it to keep
      // trying this same queue item over an over.
//      throw new RequeueException();
      throw new SuspendQueueException();
    }
    return $this->writeToEntityFields($item);
  }

  /**
   * Get the value of a single item from the row.
   *
   * @param $row
   * @param $key
   * @param $field_name
   *
   * @return mixed|null
   */
  protected function getValueFromRow($row, $key, $field_name) {
    return $row[$field_name] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityIdByKey($entity_type, $entity_bundle, $key_name, $key_value) {
    if (empty($key_value)) {
      return NULL;
    }
    $ids = \Drupal::entityQuery($entity_type)
      ->condition('type', $entity_bundle)
      ->condition($key_name . '.value', $key_value)
      // Don't need to check for access here.
      ->accessCheck(FALSE)
      ->execute();
    return ($ids) ? reset($ids) : NULL;
  }

  /**
   * Use fetched data to update entity's status.
   *
   * @param array $item
   *   Prepared item.
   *
   * @return bool
   *   TRUE if operation finished successfully.
   */
  protected function updateEntityStatus(array $item) {

    // Allow other modules to alter item.
    \Drupal::moduleHandler()->alter('real_estate_property_item_status_update', $item);

    if (!empty($item['skip_item'])) {
      // We need to skip this item.
      return TRUE;
    }

    // Add some more time with each call.
    set_time_limit(180);

    $storage = $this->entityTypeManager->getStorage($item['entity_type']);

    $key_field_value = $this->getValueFromRow($item['row'], $item['key_field_source'], $item['key_field']);
    $item['key_field_value'] = $key_field_value;
    if (!$entity_id = $this->getEntityIdByKey($item['entity_type'], $item['entity_bundle'], $item['key_field'], $key_field_value)) {
      // Item not found.
      return FALSE;
    } // Got existing entity?

    // Item exists.
    $entity = $storage->load($entity_id);
    $entity->set('update_key', '');

    $status_field_value = $this->getValueFromRow($item['row'], $item['status_field_source'], $item['status_field']);
    if ($this->saveEntityStatus($entity, $item['status_field'], $status_field_value)) {
      return TRUE;
    }
    return FALSE;

  }

  /**
   * Save entity's status.
   *
   * @param $entity
   * @param string $status_field
   * @param string $status_value
   * @param bool $always_save
   *
   * @return bool
   *   TRUE if operation finished successfully.
   */
  protected function saveEntityStatus($entity, string $status_field, string $status_value, $always_save = TRUE) {

    $changed = FALSE;
    if ($entity->hasField($status_field)) {
      if ($entity->{$status_field}->value != $status_value) {
        $changed = TRUE;
        $entity->set($status_field, $status_value);
      } // Status changed?
    } // Got a status field?

    if (!$changed && !$always_save) {
      return TRUE;
    }

    try {
      return $entity->save();
    }
    catch (\Exception $e) {
      // Log this.
      return FALSE;
    }

  }

  /**
   * Write fetched data to appropriate entity's fields.
   *
   * @param array $item
   *   Prepared item.
   *
   * @return bool
   *   TRUE if operation finished successfully.
   */
  protected function writeToEntityFields(array $item) {

//    ksm('writeToEntityFields', $data);

    // Allow other modules to alter item.
    \Drupal::moduleHandler()->alter('real_estate_property_item', $item);

    if (!empty($item['skip_item'])) {
      // We need to skip this item.
      return TRUE;
    }

    // Add some more time with each call.
    set_time_limit(180);

    $storage = $this->entityTypeManager->getStorage($item['entity_type']);

    $key_field_value = $this->getValueFromRow($item['row'], $item['key_field_source'], $item['key_field']);
    $item['key_field_value'] = $key_field_value;
    if ($entity_id = $this->getEntityIdByKey($item['entity_type'], $item['entity_bundle'], $item['key_field'], $key_field_value)) {
      // Item exists.
      $entity = $storage->load($entity_id);
    }
    else {
      // New item.
      if (!empty($item['skip_item_if_new'])) {
        // We need to skip this item.
        return TRUE;
      }
      $entity = $storage->create([
        'type' => $item['entity_bundle'],
        'title' => substr($this->t('Property data - @key', ['@key' => $this->getValueFromRow($item['row'], $item['key_field_source'], $item['key_field'])]), 0, 255),
      ]);
      if ($entity->hasField($item['key_field'])) {
        $entity->set($item['key_field'], $key_field_value);
      }
    } // Got existing entity?

    if (!empty($item['source'])) {
      $entity->set('source', $item['source']);
    }

    // Remove this since we're doing a full update.
    $entity->set('update_key', '');

    $multipart_fields = [];

    foreach ($item['select'] as $field_map) {

      $field_name = $field_map['destination'];
      if (!empty($field_map['parent'])) {
        // It's a multi-part field. Set up the array to hold all values.
        $field_name = $field_map['parent'];
        if (!isset($multipart_fields[$field_map['parent']])) {
          $multipart_fields[$field_map['parent']] = [];
        }
      }

      if ($entity->hasField($field_name)) {

        // Field exists on entity.
        $field_info = $this->getFieldInfo($item['entity_type'], $item['entity_bundle'], $field_name);
        $value = $this->getValueFromRow($item['row'], $field_map['source'], $field_map['destination']);
        $this->updateValuesForFieldType($value, $field_info, $item);

        if (!empty($field_map['parent'])) {
          // Save multi-part fields for later.
          $parents = explode('/', $field_map['destination']);
          $top_key = array_shift($parents);
          NestedArray::setValue($multipart_fields[$field_map['parent']], $parents, $value);
        }
        else {
          // Set single-value fields now.
          $entity->set($field_name, $value);
        }
      }

    } // Loop thru mapped fields.

    if ($multipart_fields) {
      // Now, set the multi-part fields, each one all at once.
      foreach ($multipart_fields as $field_name => $value) {
        $entity->set($field_name, $value);
      }
    }

    try {
      $saved = $entity->save();

      // Allow other modules to act on saved property entity.
      \Drupal::moduleHandler()->invokeAll('real_estate_property_property_post_save', [$entity, $item]);

      return $saved;
    }
    catch (\Exception $e) {
      // Log this.
      return FALSE;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function numberOfQueueItems(string $type) {
    $queue = ($type == 'fetch') ? $this->fetchQueue : $this->itemQueue;
    return $queue->numberOfItems();
  }

  /**
   * {@inheritdoc}
   */
  public function claimQueueItem(string $type) {
    $queue = ($type == 'fetch') ? $this->fetchQueue : $this->itemQueue;
    return $queue->claimItem();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteQueueItem(string $type, \stdClass $item) {
    $queue = ($type == 'fetch') ? $this->fetchQueue : $this->itemQueue;
    return $queue->deleteItem($item);
  }

  /**
   * Get information about a field.
   *
   * @param $entity_type_id
   * @param $bundle
   * @param $field_name
   *
   * @return array
   *   Array with field type, cardinality, and settings (for files).
   */
  protected function getFieldInfo($entity_type_id, $bundle, $field_name) {
    static $types = [];
    if (!isset($types[$field_name])) {
      $types[$field_name] = NULL;
      if ($config = FieldStorageConfig::loadByName($entity_type_id, $field_name)) {
        $types[$field_name] = [
          'type' => $config->getType(),
          'cardinality' => $config->getCardinality(),
          'settings' => [],
          ];
        switch ($config->getType()) {
          case 'image':
          case 'file':
            // Load the field config for file types.
            if ($config = FieldConfig::loadByName($entity_type_id, $bundle, $field_name)) {
              $types[$field_name]['settings'] = $config->getSettings();
            }
            break;
          case 'text':
          case 'text_long':
          case 'text_with_summary':
            if ($config = FieldConfig::loadByName($entity_type_id, $bundle, $field_name)) {
              if ($default = $config->getDefaultValueLiteral()) {
                $types[$field_name]['settings']['default'] = $default[0];
              }
              else {
                $types[$field_name]['settings']['default'] = [
                  'value' => '',
                  'format' => filter_default_format(),
                ];
              }
            }
        }
      }
    }
    return $types[$field_name];
  }

  /**
   * Updates data values based on the type of field it will be, looping through
   * an array, if necessary.
   *
   * @param mixed $values
   * @param array $field_info
   * @param array $item
   */
  protected function updateValuesForFieldType(&$values, $field_info, $item) {
    $cardinality = $field_info['cardinality'] ?? 1;
    if (is_array($values) && $cardinality != 1) {
      foreach ($values as $index => $value) {
        $this->updateValueForFieldType($values[$index], $field_info, $item);
      } // Loop thru each value.
    }
    else {
      // Only use a single value.
      $this->updateValueForFieldType($values, $field_info, $item);
    }
  }

  /**
   * Updates a single data value based on the type of field it will be.
   *
   * @param mixed $value
   * @param array $field_info
   * @param array $item
   */
  protected function updateValueForFieldType(&$value, $field_info, $item) {
    $field_type = $field_info['type'] ?? NULL;
    switch ($field_type) {
      case 'boolean':
        switch (strtolower($value)) {
          case 'false':
            $value = FALSE;
            break;
          case 'true':
            $value = TRUE;
            break;
          default:
            $value = (boolean) $value;
        }
        break;
      case 'datetime':
        $timestamp = ($value) ? strtotime($value) : FALSE;
        if ($timestamp !== FALSE) {
          $datetime = new DrupalDateTime('@' . $timestamp, DateTimeItemInterface::STORAGE_TIMEZONE);
          $value = $datetime->format('Y-m-d\TH:i:s');
        }
        else {
          $value = NULL;
        }
        break;
      case 'text':
      case 'text_long':
      case 'text_with_summary':
        $value = [
          'value' => $value,
          'format' => $field_info['settings']['default']['format'],
        ];
        break;
      case 'file':
        if ($file_fid = $this->getRemoteFile($value, $field_info, $item)) {
          $value = [
            'target_id' => $file_fid,
          ];
        }
        break;
      case 'image':
        if ($image_fid = $this->getRemoteFile($value, $field_info, $item)) {
          $value = [
            'target_id' => $image_fid,
            'alt' => $value['alt'] ?? NULL,
          ];
        }
        else {
          $value = NULL;
        }
        break;
      default:
        // Do nothing.
    }
  }

  /**
   * @param $url
   * @param $field_info
   *
   * @return bool|int
   */
  protected function getRemoteFile($url, $field_info, $item) {

    $delete_temp_file = $url['delete_temp_file'] ?? FALSE;
    $url = $url['url'] ?? $url;

    // Retrieve using URL.
    $parts = parse_url($url);
    if (empty($parts['scheme']) || empty($parts['host'])) {
      return FALSE;
    }
    $context = stream_context_create(['http' => ['timeout' => 15]]);
    $file_contents = @file_get_contents(str_replace(' ', '%20', $url), NULL, $context);
//  $file_contents = @file_get_contents('themes/custom/lfr_base/assets/images/property-image-load.jpg', NULL, $context);

    $headers = $this->getHeaders(($http_response_header ?? []));

    if ($delete_temp_file) {
      // Delete the temp file.
      $this->fileSystem->delete($url);
    } // Got a temp file to delete?

    if (!$file_contents) {
      // @todo - Use dependency injection here.
      \Drupal::messenger()->addError(t('Could not retrieve URL [@url]. Response code: @code', ['@url' => $url, '@code' => $headers['response_code']]));
      \Drupal::logger('lafr')->error('Could not retrieve URL [@url]. Response code: @code', ['@url' => $url, '@code' => $headers['response_code']]);
      return FALSE;
    }

    $mime_type_extension = str_replace('image/', '', ($headers['mime_type'] ?? ''));
    $mime_type_filename = $this->t('fetched-image@ext', ['@ext' => (($mime_type_extension) ? '.' . $mime_type_extension : '')]);

    $path_extension = '';
    $path_filename = '';

    if (!empty($parts['path'])) {
      $parts = pathinfo($parts['path']);
      $path_filename = $parts['basename'] ?? NULL;
      $path_extension = $parts['extension'] ?? NULL;
      if ($path_filename && !$path_extension && $mime_type_extension) {
        $path_filename .= '.' . $mime_type_extension;
        $path_extension = $mime_type_extension;
      }
    }

    $filename = '';
    $extension = '';
    $ext_valid = FALSE;

    if ($path_filename && $ext_valid = $this->extensionIsValid($path_extension, $field_info)) {
      $filename = $path_filename;
      $extension = $path_extension;
    }
    if (!$ext_valid && $mime_type_extension && $ext_valid = $this->extensionIsValid($mime_type_extension, $field_info)) {
      $filename = $mime_type_filename;
      $extension = $mime_type_extension;
    }

    if (!$ext_valid) {
      // Invalid file extension.
      \Drupal::messenger()->addError(t('Invalid file extension [@ext] for URL [@url], Item @key', ['@ext' => $extension, '@url' => $url, '@key' => $item['key_field_value']]));
      \Drupal::logger('lafr')->error('Invalid file extension [@ext] for URL [@url], Item @key', ['@ext' => $extension, '@url' => $url, '@key' => $item['key_field_value']]);
      return FALSE;
    }

    // @todo - validate $field_info['settings']['max_filesize']

    $token_service = \Drupal::token();
    $file_info['path'] = $token_service->replace($field_info['settings']['file_directory']);
    $file_info['uri_scheme'] = $field_info['settings']['uri_scheme'];
    $file_info['filename'] = $filename;

    // Allow other modules to alter filename, path.
    \Drupal::moduleHandler()->alter('real_estate_property_item_file_info', $file_info, $item);

    $file_info['path'] = $file_info['uri_scheme'] . '://' . $file_info['path'];

    if (!$this->fileSystem->prepareDirectory($file_info['path'], FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      // @todo - add logging/error message.
      return FALSE;
    }

    $destination = $file_info['path'] . '/' . $file_info['filename'];
    $file = file_save_data($file_contents, $destination, FileSystemInterface::EXISTS_RENAME);

    return ($file) ? $file->id() : FALSE;

  }

  /**
   * Checks if specified extension is valid for the given field settings.
   *
   * @param $extension
   * @param $field_info
   *
   * @return bool
   */
  protected function extensionIsValid($extension, $field_info) {
    if (!empty($field_info['settings']['file_extensions'])) {
      $file_extensions = array_filter(explode(' ', $field_info['settings']['file_extensions']));
      if (!$extension || !in_array(strtolower($extension), $file_extensions)) {
        // Invalid file extension.
        return FALSE;
      }
    }
    return TRUE;
  }

  protected function getHeaders($http_response_header) {
    $headers = [
      'response_code' => $response_code ?? NULL,
      'mime_type' => NULL,
    ];
    $all = '|' . strtolower(implode('|', $http_response_header)) . '|';
    if (preg_match_all('/\|http\/.*? (.*?)(\||\s)/', $all, $matches)) {
      $headers['response_code'] = array_pop($matches[1]);
    }
    if (preg_match_all('/\|content-type: (.*?)\|/', $all, $matches)) {
      $headers['mime_type'] = explode(';', array_pop($matches[1]))[0];
    }
    return $headers;
  }

}
