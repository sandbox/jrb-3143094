<?php

namespace Drupal\real_estate_property;

/**
 * A query value object.
 */
class PropertyQuery implements PropertyQueryInterface {

  /**
   * The query's ID.
   *
   * @var string
   */
  private $id;

  /**
   * The query's label.
   *
   * @var string
   */
  private $label;

  /**
   * The query's weight.
   *
   * @var string
   */
  private $weight;

  /**
   * The query's cron_interval.
   *
   * @var string
   */
  private $cronInterval;

  /**
   * The query's status_cron_interval.
   *
   * @var string
   */
  private $statusCronInterval;

  /**
   * The query's key_field.
   *
   * @var string
   */
  private $keyField;

  /**
   * The query's status_field.
   *
   * @var string
   */
  private $statusField;

  /**
   * The query's entity.
   *
   * @var string
   */
  private $entity;

  /**
   * The query's select.
   *
   * @var string
   */
  private $select;

  /**
   * PropertyQuery constructor.
   *
   * @param string $id
   *   The query's ID.
   * @param string $label
   *   The query's label.
   * @param string $weight
   *   The query's weight.
   * @param string $cron_interval
   *   The query's cron_interval.
   * @param string $status_cron_interval
   *   The query's status_cron_interval.
   * @param string $key_field
   *   The query's key_field.
   * @param string $status_field
   *   The query's status_field.
   * @param string $entity
   *   The query's entity.
   * @param string $select
   *   The query's select.
   */
  public function __construct($id, $label, $weight, $cron_interval, $status_cron_interval, $key_field, $status_field, $entity, $select) {
    $this->id = $id;
    $this->label = $label;
    $this->weight = $weight;
    $this->cronInterval = $cron_interval;
    $this->statusCronInterval = $status_cron_interval;
    $this->keyField = $key_field;
    $this->statusField = $status_field;
    $this->entity = $entity;
    $this->select = $select;
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function weight() {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function cronInterval() {
    return $this->cronInterval;
  }

  /**
   * {@inheritdoc}
   */
  public function statusCronInterval() {
    return $this->statusCronInterval;
  }

  /**
   * {@inheritdoc}
   */
  public function keyField() {
    return $this->keyField;
  }

  /**
   * {@inheritdoc}
   */
  public function statusField() {
    return $this->statusField;
  }

  /**
   * {@inheritdoc}
   */
  public function entity() {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function select() {
    return $this->select;
  }

}
