<?php

namespace Drupal\real_estate_property;

/**
 * Processor of property data.
 */
interface PropertyProcessorInterface {

  /**
   * Claims an item in the fetch queue for processing.
   *
   * @param string $type
   *   Type of queue (fetch/item).
   *
   * @return bool|\stdClass
   *   On success we return an item object. If the queue is unable to claim an
   *   item it returns false.
   *
   * @see \Drupal\Core\Queue\QueueInterface::claimItem()
   */
  public function claimQueueItem(string $type);

  /**
   * Attempts to drain the queue of tasks for release history data to fetch.
   */
  public function fetchData();

  /**
   * Attempts to drain the queue of tasks for item processing.
   */
  public function fetchItem();

  /**
   * Adds a fetch task to the queue.
   *
   * @param array $query
   *   Associative array of information about the query to fetch data for.
   */
  public function createFetchTask(array $query);

  /**
   * Adds an item task to the queue.
   *
   * @param array $query
   *   Associative array of information about the item to process.
   */
  public function createItemTask(array $query);

  /**
   * Processes a task to fetch available data for a single query.
   *
   * @param array $query
   *   Associative array of information about the query to fetch data for.
   *
   * @return bool
   *   TRUE if we fetched passable XML, otherwise FALSE.
   */
  public function processFetchTask(array $query);

  /**
   * Processes a task to create a single item.
   *
   * @param array $data
   *   Associative array of information on the item.
   *
   * @return bool
   *   TRUE if we created/updated the item, otherwise FALSE.
   */
  public function processItemTask(array $data);

  /**
   * Retrieves the number of items in the fetch queue.
   *
   * @param string $type
   *   Type of queue (fetch/item).
   *
   * @return int
   *   An integer estimate of the number of items in the queue.
   *
   * @see \Drupal\Core\Queue\QueueInterface::numberOfItems()
   */
  public function numberOfQueueItems(string $type);

  /**
   * Deletes a finished item from the fetch queue.
   *
   * @param string $type
   *   Type of queue (fetch/item).
   *
   * @param \stdClass $item
   *   The item returned by \Drupal\Core\Queue\QueueInterface::claimItem().
   */
  public function deleteQueueItem(string $type, \stdClass $item);

  /**
   * Find entity ID based on key name/value.
   *
   * @param $entity_type
   * @param $entity_bundle
   * @param $key_name
   * @param $key_value
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The matching entity.
   */
  public function getEntityIdByKey($entity_type, $entity_bundle, $key_name, $key_value);

}
