<?php

namespace Drupal\real_estate_property;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;

/**
 * Fetches a data from a source.
 */
abstract class PropertyFetcher implements PropertyFetcherInterface {

  use DependencySerializationTrait;

  /**
   * Constructs a PropertyFetcher.
   */
  public function __construct() {
  }

  /**
   * Returns a formatted mapping array for source -> Drupal fields.
   *
   * Multi-part fields (e.g. address) can be specified as:
   *   * ./City:field_address/locality
   * ./PostalCode:field_address/postal_code
   *
   * Processors can be specified as:
   *   * ./property_pdf:field_property_pdf_list|explode:,
   * ./property_pdf:field_property_image|explode:,|other_action:a=1:b=23
   *
   * In processors, the characters : and = should be escaped as \: and \=
   * For example, to explode on ":":
   * ./property_pdf:field_property_pdf_list|explode:\:
   *
   * @param $select
   *
   * @return array
   */
  protected function parseSelectMapping($select) {

    $mapping = [];

    // Replace any escaped ":".
    $select = str_replace('\:', chr(30), $select);
    $mapping_raw = array_filter(array_map('trim', preg_split('/\R/', $select)));

    foreach ($mapping_raw as $line) {

      $fields = explode(':', $line);
      if (empty($fields[1])) {
        continue;
      }

      $source = array_shift($fields);
      $destination = implode(':', $fields);

      $parts = explode('|', $destination);
      $destination_name = array_shift($parts);

      $processors = [];
      if (!empty($parts)) {
        foreach ($parts as $part) {
          $processor_parts = explode(':', $part);
          $processor_name = array_shift($processor_parts);
          $args = [];
          if (!empty($processor_parts)) {
            foreach ($processor_parts as $processor_part) {
              // Return any escaped ":".
              $processor_part = str_replace(chr(30), ':', $processor_part);
              // Replace any escaped "=".
              $processor_part = str_replace('\=', chr(30), $processor_part);

              $arg_parts = explode('=', $processor_part);
              foreach ($arg_parts as &$arg_part) {
                // Return any escaped "=".
                $arg_part = str_replace(chr(30), '=', $arg_part);
              } // Loop thru parts of the arg.
              if (!empty($arg_parts[1])) {
                $args[$arg_parts[0]] = $arg_parts[1];
              }
              else {
                $args[] = $arg_parts[0];
              }
            } // Loop thru processor args.
          } // Got processor args?

          $processors[] = [
            'name' => $processor_name,
            'args' =>$args,
          ];

        } // Loop thru parts of destination (processors).

      } // Got parts of the destination (processors)?

      $destination_name_parts = explode('/', $destination_name);
      $mapping[] = [
        'source' => $source,
        'destination' => $destination_name,
        'parent' => (!empty($destination_name_parts[1])) ? $destination_name_parts[0] : NULL,
        'processors' => $processors,
      ];

    } // Loop thru lines.

    return $mapping;

  }

  /**
   * Runs processors specified in mapping on a value.
   *
   * @param mixed $value
   * @param array $field_map
   */
  protected function runFieldProcessors(&$value, $field_map) {
    if (empty($field_map['processors'])) {
      return;
    }
    foreach ($field_map['processors'] as $processor) {
      switch ($processor['name']) {
        case 'default_value':
          $this->runDefaultValueProcessor($value, $processor);
          break;
        case 'explode':
          $this->runExplodeProcessor($value, $processor);
          break;
      }
    } // Loop thru processors.
  }

  /**
   * Runs default_value processor on a value.
   *
   * @param mixed $value
   * @param array $settings
   */
  protected function runDefaultValueProcessor(&$value, $settings) {
    $default_value = $settings['args'][0] ?? NULL;
    $value = $value ?? $default_value;
  }

  /**
   * Runs explode processor on a value.
   *
   * @param mixed $value
   * @param array $settings
   */
  protected function runExplodeProcessor(&$value, $settings) {
    $delimiter = $settings['args'][0] ?? "\n";
    $value = explode($delimiter, $value);
  }

}
