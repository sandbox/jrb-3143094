<?php

namespace Drupal\real_estate_property\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\real_estate_property\PropertyProcessorInterface;

/**
 * Provides base functionality for the Property Queue Workers.
 */
abstract class PropertyQueueWorkerBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * PropertyProcessorInterface definition.
   *
   * @var \Drupal\real_estate_property\PropertyProcessorInterface
   */
  protected $propertyProcessor;

  /**
   * LinkRefresherBase constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\real_estate_property\PropertyProcessorInterface $property_processor
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PropertyProcessorInterface $property_processor) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->propertyProcessor = $property_processor;
  }

}
