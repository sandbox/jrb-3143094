<?php

namespace Drupal\real_estate_property;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Default implementation of PropertyManagerInterface.
 */
abstract class PropertyManager implements PropertyManagerInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * Type string used in messages.
   *
   * @var string
   */
  protected $managerType;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configSettings;

  /**
   * PropertyProcessorInterface definition.
   *
   * @var \Drupal\real_estate_property\PropertyProcessorInterface
   */
  protected $propertyProcessor;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * An array of available queries.
   *
   * @var array
   */
  protected $queries;

  /**
   * Constructs a PropertyManager.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\real_estate_property\PropertyProcessorInterface $property_processor
   *   The Property Processor service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, PropertyProcessorInterface $property_processor, EntityTypeManagerInterface $entity_type_manager) {
    $this->configSettings = $config_factory->get($this->configObjectName);;
    $this->propertyProcessor = $property_processor;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function refreshData($queries_to_run = NULL, $status_update = FALSE) {

    $query_sets = $this->getQueries();

    foreach ($query_sets as $query_set) {
      $feed = array_slice($query_set, 0, -1);
      foreach ($query_set['queries'] as $key => $query) {
        $query = array_merge($feed, ['query_id' => $key], $query);
        $query['status_update'] = $status_update;
        if (is_null($queries_to_run) || !empty($queries_to_run[$query_set['id']][$key])) {
          $this->propertyProcessor->createFetchTask($query);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getQueries() {

    $connects = [];

    $storage = $this->entityTypeManager->getStorage($this->entityTypeId);
    $query = $storage->getQuery();

    $qids = $query->execute();

    $this->queries = $this->entityTypeManager->getStorage($this->entityTypeId)->loadMultiple($qids);

    foreach ($this->queries as $connect) {
      $connects[] = $connect->toArray();
    }

    return $connects;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchDataBatch(array &$context) {

    if (empty($context['sandbox']['max'])) {
      $context['finished'] = 0;
      $context['sandbox']['max'] = $this->propertyProcessor->numberOfQueueItems('fetch');
      $context['sandbox']['progress'] = 0;
      $context['message'] = $this->t('Loading @type data ...', ['@type' => $this->getManagerTypeText()]);
      $context['results']['queries']['updated'] = 0;
      $context['results']['queries']['failures'] = 0;
      $context['results']['queries']['processed'] = 0;
    }

    // Grab another item from the fetch queue.
    for ($i = 0; $i < 5; $i++) {
      if ($item = $this->propertyProcessor->claimQueueItem('fetch')) {
        $args =  [
          '@type' => $this->getManagerTypeText(),
          '%title' => $item->data['label'],
          '@num' => $context['sandbox']['progress'] + 1,
          '@max' => $context['sandbox']['max'],
        ];
        if ($this->propertyProcessor->processFetchTask($item->data)) {
          $context['results']['queries']['updated']++;
          $context['message'] = $this->t('Fetching @type data for %title (@num/@max).', $args);
        }
        else {
          $context['message'] = $this->t('Failed to fetch @type data for %title (@num/@max).', $args);
          $context['results']['queries']['failures']++;
        }
        $context['sandbox']['progress']++;
        $context['results']['queries']['processed']++;
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
        $this->propertyProcessor->deleteQueueItem('fetch', $item);
      }
      else {
        $context['finished'] = 1;
        return;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function itemBatch(array &$context) {

    if (empty($context['sandbox']['max'])) {
      $context['finished'] = 0;
      $context['sandbox']['max'] = $this->propertyProcessor->numberOfQueueItems('item');
      $context['sandbox']['progress'] = 0;
      $context['message'] = $this->t('Processing @type item ...', ['@type' => $this->getManagerTypeText()]);
      $context['results']['items']['updated'] = 0;
      $context['results']['items']['failures'] = 0;
      $context['results']['items']['processed'] = 0;
    }

    // Grab another item from the fetch queue.
    for ($i = 0; $i < 5; $i++) {
      if ($item = $this->propertyProcessor->claimQueueItem('item')) {
        $id = $item->data['row'][$item->data['key_field']] ?? '?';
        $args =  [
          '%id' => $id,
          '@num' => $context['sandbox']['progress'] + 1,
          '@max' => $context['sandbox']['max'],
        ];
        if ($this->propertyProcessor->processItemTask($item->data)) {
          $context['results']['items']['updated']++;
          $context['message'] = $this->t('Processing item data for %id (@num/@max).', $args);
        }
        else {
          $context['message'] = $this->t('Failed to process item data for %id (@num/@max).', $args);
          $context['results']['items']['failures']++;
        }
        $context['sandbox']['progress']++;
        $context['results']['items']['processed']++;
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
        $this->propertyProcessor->deleteQueueItem('item', $item);
      }
      else {
        $context['finished'] = 1;
        return;
      }
    }
  }

}
