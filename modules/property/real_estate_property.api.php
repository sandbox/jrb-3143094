<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * Allow other modules to change full data set.
 *
 * If $data_set['data'][{delta}]['skip_item'] is set to TRUE, item will be skipped.
 *
 * @param $data_set
 */
function hook_real_estate_property_data_set_alter(&$data_set) {
  foreach ($data_set['data'] as &$row) {
    // Take data from multiple fields and put it in a single array to use with
    // address field.
    $row['field_address'] = [
      'country_code' => 'US',
      'administrative_area' => $row['state'],
      'locality' => $row['city'],
      'postal_code' => $row['zip'],
      'address_line1' => $row['address'],
      'address_line2' => $row['address2'],
    ];
  }
}

/**
 * Allow other modules to change single item data.
 *
 * If $item['skip_item'] is set to TRUE, item will be skipped.
 *
 * @param $item
 */
function hook_real_estate_property_item_alter(&$item) {
  // Take data from multiple fields and put it in a single array to use with
  // address field.
  $item['row']['field_address'] = [
    'country_code' => 'US',
    'administrative_area' => $item['row']['state'],
    'locality' => $item['row']['city'],
    'postal_code' => $item['row']['zip'],
    'address_line1' => $item['row']['address'],
    'address_line2' => $item['row']['address2'],
  ];
  // Skip items in certain conditions.
  if ($item['row']['my_value'] == 'skip') {
    $item['skip_item'] = TRUE;
  }
}

/**
 * Allow other modules to change remote file item path data.
 *
 * @param array $file_info
 *   Array with keys: uri_scheme, path, filename
 * @param array $item
 */
function hook_real_estate_property_item_file_info_alter(&$file_info, $item) {
  // Append "/imported" to the usual file path.
  $file_info['path'] .= '/imported';
}

/**
 * Allow other modules to act on saved property entity.
 *
 * @param $entity
 * @param $item
 */
function hook_real_estate_property_property_post_save($entity, $item) {
  if ($entity->isNew()) {
    // Do something to new entities.
  }
}
