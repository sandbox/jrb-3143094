Drupal Real Estate (DRE) 
===========================

This code is a work-in-progress.  While it isn't complete, it is currently
being used successfully on a production website.

### Mapping RETS fields to Drupal fields.

The module supports mapping fields from the RETS feed to Drupal entity 
fields.  The format for each mapping is `rets_field:drupal_field_name`.

For example, this is a full mapping we're using now:
```
LIST_1:key
LIST_105:field_mls_number
LIST_105:field_global_property_id
LIST_34:title
LIST_230:field_description
LIST_22:field_price
LIST_31:street_number
LIST_37:street_suffix
LIST_34:field_address/address_line1
LIST_39:field_address/locality
LIST_40:field_address/administrative_area
LIST_43:field_address/postal_code
LIST_46:field_latitude
LIST_47:field_longitude
:field_location
LIST_41:field_county
LIST_15:field_property_status
:field_address/country_code|default_value:US
LIST_57:field_acreage
listing_office_name:listing_office_name
:field_featured_image
:field_property_attachment
:field_property_category
:field_listing_source|default_value:mls_lot
:field_rets_data_source|default_value:RealtyCo - B - Lots and Land
listing_office_name:field_mls_company
:user_id|default_value:1
:status|default_value:1
```

Multi-part fields (e.g. address) can be specified as:
```
./City:field_address/locality
./PostalCode:field_address/postal_code
```

The mapping process supports some processing, including default values
and exploding on a character. Processors can be specified as:
```
./property_pdf:field_property_pdf_list|explode:,
:status|default_value:1
```

In processors, the characters `:` and `=` should be escaped as `\:`and `\=`
For example, to explode on ":":
```
./property_pdf:field_property_pdf_list|explode:\:
```

@TODO - The format of this mapping information should be switched to
YAML rather than this custom format.

### Drupal hooks to alter data

The module supports various hooks to allow developers to modify RETS
data as it's being processed.  For example, you could map RETS status
values to different values used by the Drupal site and ignore listings
with certain statuses.

```php
/**
 * Implements hook_real_estate_property_item_alter().
 */
function landandfarms_system_real_estate_property_item_alter(&$item) {
  // Translate some status values.
  switch ($item['row']['field_property_status']) {
    case 'Coming Soon':
    case 'New Listing':
      $item['row']['field_property_status'] = 'Active';
      break;
    case 'Pending with Showings':
    case 'Active Under Contract':
    case 'Under Contract':
    case 'Active Contingency':
      $item['row']['field_property_status'] = 'Pending';
      break;
    case 'Closed':
    case 'Closed/Sold':
      $item['row']['field_property_status'] = 'Sold';
      break;
    case 'Withdrawn/Release':
    case 'Off Market':
    case 'Rented':
    case 'Withdrawn':
      // Ignore listings with this status.
      $item['skip_item'] = TRUE;
      break;
  }
}
```

### Automated processing of RETS feeds

RETS queries can be set to run via Drupal's cron. That will queue the
retrieval of the RETS data.  Drupal's queue processing will then need
to be called via Cron on the server to actually (1) make the RETS calls
and (2) process the items retrieved by the RETS calls.  For example,
you could have a bash script like this that runs periodically.

```shell
# Run fetch for manually-queued fetch tasks.
drush queue:run --time-limit=120 real_estate_rets_item_tasks;

# Run fetch for cron fetch tasks.
drush queue:run --time-limit=120 real_estate_rets_fetch_tasks_cron;

# Process queued property listings.
drush queue:run --time-limit=120 real_estate_rets_item_tasks_cron;
```
